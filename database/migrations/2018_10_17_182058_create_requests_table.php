<?php
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateRequestsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('requests', function (Blueprint $table) {
                $table->increments('id');

                $table->string('action');
                $table->integer('user_id')->unsigned();
                $table->string('token');

                $table->text('data')->nullable();

                $table->boolean('is_completed')->default(false);

                $table->timestamp('expires_at');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('requests');
        }
    }
