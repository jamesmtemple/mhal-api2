<?php
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUsersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');

                $table->string('ein')->unique();

                $table->text('username')->nullable();
                $table->text('email');
                $table->string('_username')->nullable();
                $table->string('_email');
                $table->string('password');

                $table->boolean('is_verified')->default(false);
                $table->string('backup_code')->nullable();

                $table->rememberToken();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('users');
        }
    }
