<?php
    use Faker\Generator as Faker;
    use App\Models\User;

    /*
    |--------------------------------------------------------------------------
    | Model Factories
    |--------------------------------------------------------------------------
    |
    | This directory should contain each of the model factory definitions for
    | your application. Factories provide a convenient way to generate new
    | model instances for testing / seeding your application's database.
    |
    */

    $factory->define(User::class, function (Faker $faker) {
            $firstName = $faker->firstName;
            $lastName  = $faker->lastName;

            return [
                'ein'                 => $faker->numberBetween(1000,9999),
                'username'            => strtolower($firstName[0] . $lastName),
                'email'               => $faker->unique()->safeEmail,
                'password'            => "4pAn5bO9ZMzF5wzmJLn2"
            ];
      });
