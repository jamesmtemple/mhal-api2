<?php
  Route::name("accounts.")->namespace("Accounts")->prefix('accounts')->group(function(){
      Route::get("activate/{request}", "ActivationsController@store")->name("activate");

      Route::post("register", "AccountsController@store")->name("register");
      Route::post("login", "SessionsController@store")->name("login");
      Route::post("settings", "AccountsController@update")->name("settings");
  });

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
