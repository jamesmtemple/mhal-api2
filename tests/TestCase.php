<?php namespace Tests;
    use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
    use Laravel\Passport\ClientRepository;
    use Illuminate\Support\Facades\DB;
    use Carbon\Carbon;

    abstract class TestCase extends BaseTestCase
    {
        use CreatesApplication;

        protected function setup(){
            parent::setup();

            $clientRepository = new ClientRepository;

            $client = $clientRepository
              ->createPersonalAccessClient(null, 'Test Personal Access Token','http://localhost');

            DB::table('oauth_personal_access_clients')
              ->insert([
                ['client_id' => $client->id, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
              ]);
        }
    }
