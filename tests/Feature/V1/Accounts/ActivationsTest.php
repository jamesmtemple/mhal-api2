<?php namespace Tests\Feature\V1\Accounts;
    use Tests\TestCase;
    use Illuminate\Foundation\Testing\WithFaker;
    use Illuminate\Foundation\Testing\RefreshDatabase;
    use App\Models\User;
    use App\Models\Request;

    class ActivationsTest extends TestCase
    {
        use RefreshDatabase;

        /** @test */
        public function a_user_must_activate_their_account()
        {
              $record = factory(User::class)
                ->create()
                ->fresh();

              $this->assertFalse($record->is_verified);
        }

        /** @test */
        public function a_user_can_activate_their_account()
        {
            $attributes = factory(User::class)
              ->raw();

            $this->postJson(route('accounts.register'), $attributes);

            $request = Request::first();

            $this->getJson(route('accounts.activate', $request))
              ->assertStatus(200)
              ->assertJsonStructure(['status']);
        }
    }
