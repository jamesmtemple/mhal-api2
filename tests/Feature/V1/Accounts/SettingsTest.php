<?php namespace Tests\Feature\V1\Accounts;
    use Tests\TestCase;
    use Illuminate\Foundation\Testing\WithFaker;
    use Illuminate\Foundation\Testing\RefreshDatabase;
    use App\Models\User;
    use Laravel\Passport\Passport;

    class SettingsTest extends TestCase
    {
        use RefreshDatabase;

        /** @test */
        public function a_user_must_provide_an_employee_id()
        {
            $attributes = factory(User::class)
            ->raw(['ein' => '']);

            Passport::actingAs(factory(User::class)->create());

            $this->postJson(route('accounts.settings'), $attributes)
              ->assertStatus(422)
              ->assertJsonValidationErrors("ein");
        }

        /** @test */
        public function a_user_must_provide_a_username()
        {
          $attributes = factory(User::class)
            ->raw(['username' => '']);

          Passport::actingAs(factory(User::class)->create());

          $this->postJson(route('accounts.settings'), $attributes)
            ->assertStatus(422)
            ->assertJsonValidationErrors("username");
        }

        /** @test */
        public function a_users_username_must_be_unique()
        {
            $attributes = factory(User::class)
              ->raw();

            Passport::actingAs(factory(User::class)->create());

            $model = User::create($attributes);

            $this->postJson(route('accounts.settings'), $attributes)
              ->assertStatus(422)
              ->assertJsonValidationErrors("username");
        }

        /** @test */
        public function a_user_must_provide_an_email()
        {
            $attributes = factory(User::class)
              ->raw(['email' => '']);

            Passport::actingAs(factory(User::class)->create());

            $this->postJson(route('accounts.settings'), $attributes)
              ->assertStatus(422)
              ->assertJsonValidationErrors("email");
        }

        /** @test */
        public function a_users_email_must_be_unique()
        {
          $attributes = factory(User::class)
            ->raw();

          $model = User::create($attributes);

          Passport::actingAs(factory(User::class)->create());

          $this->postJson(route('accounts.settings'), $attributes)
            ->assertStatus(422)
            ->assertJsonValidationErrors("email");
        }

        /** @test */
        public function a_user_must_provide_a_current_password()
        {
            $attributes = factory(User::class)
              ->raw(['email' => '']);

            Passport::actingAs(factory(User::class)->create());

            $this->postJson(route('accounts.settings'), $attributes)
              ->assertStatus(422)
              ->assertJsonValidationErrors("current_password");
        }

        /** @test */
        public function a_user_can_update_their_account_settings()
        {
            $attributes = factory(User::class)
              ->raw();

            Passport::actingAs(factory(User::class)->create());

            $this->postJson(route('accounts.settings'), array_merge($attributes,['current_password' => '4pAn5bO9ZMzF5wzmJLn2']))
              ->assertStatus(200)
              ->assertJsonStructure(['status']);
        }
    }
