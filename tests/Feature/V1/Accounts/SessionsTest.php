<?php namespace Tests\Feature\V1\Accounts;
    use Tests\TestCase;
    use Illuminate\Foundation\Testing\WithFaker;
    use Illuminate\Foundation\Testing\RefreshDatabase;
    use App\Models\User;

    class SessionsTest extends TestCase
    {
        use RefreshDatabase;

        /** @test */
        public function a_username_is_required()
        {
            $this->postJson(route("accounts.login"), ['password' => 'testpass'])
              ->assertStatus(422)
              ->assertJsonValidationErrors("username");
        }

        /** @test */
        public function a_password_is_required()
        {
            $this->postJson(route("accounts.login"), ['username' => 'usernam'])
              ->assertStatus(422)
              ->assertJsonValidationErrors("password");
        }

        /** @test */
        public function account_activation_is_required()
        {
            $record = factory(User::class)
              ->create();

            $this->postJson(route("accounts.login"), ['username' => $record->username, 'password' => '4pAn5bO9ZMzF5wzmJLn2'])
              ->assertStatus(401)
              ->assertJsonStructure(['status']);
        }

        /** @test */
        public function a_user_can_sign_in_with_valid_credentials()
        {
            $record = factory(User::class)
              ->create(['is_verified' => true]);

            $this->postJson(route("accounts.login"), ['username' => $record->username, 'password' => '4pAn5bO9ZMzF5wzmJLn2'])
              ->assertStatus(200)
              ->assertJsonStructure(['status','auth']);
        }

        /** @test */
        public function a_user_cannot_sign_in_with_invalid_credentials()
        {
          $record = factory(User::class)
            ->create();

          $this->postJson(route("accounts.login"), ['username' => $record->username, 'password' => 'tsting'])
            ->assertStatus(401)
            ->assertJsonStructure(['status']);
        }
    }
