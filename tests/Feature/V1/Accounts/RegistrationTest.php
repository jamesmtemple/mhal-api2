<?php namespace Tests\Feature\V1\Accounts;
    use Tests\TestCase;
    use Illuminate\Foundation\Testing\WithFaker;
    use Illuminate\Foundation\Testing\RefreshDatabase;
    use App\Models\User;

    class RegistrationTest extends TestCase
    {
        use RefreshDatabase;

        /** @test */
        public function a_user_must_provide_an_employee_id()
        {
            $attributes = factory(User::class)
              ->raw(['ein' => '']);

            $this->postJson(route('accounts.register'), $attributes)
              ->assertStatus(422)
              ->assertJsonValidationErrors("ein");
        }

        /** @test */
        public function a_user_must_provide_a_username()
        {
          $attributes = factory(User::class)
            ->raw(['username' => '']);

          $this->postJson(route('accounts.register'), $attributes)
            ->assertStatus(422)
            ->assertJsonValidationErrors("username");
        }

        /** @test */
        public function a_users_username_must_be_unique()
        {
          $attributes = factory(User::class)
            ->raw();

          $model = User::create($attributes);

          $this->postJson(route('accounts.register'), $attributes)
            ->assertStatus(422)
            ->assertJsonValidationErrors("username");
        }

        /** @test */
        public function a_user_must_provide_an_email()
        {
          $attributes = factory(User::class)
            ->raw(['email' => '']);

          $this->postJson(route('accounts.register'), $attributes)
            ->assertStatus(422)
            ->assertJsonValidationErrors("email");
        }

        /** @test */
        public function a_users_email_must_be_unique()
        {
          $attributes = factory(User::class)
            ->raw();

          $model = User::create($attributes);

          $this->postJson(route('accounts.register'), $attributes)
            ->assertStatus(422)
            ->assertJsonValidationErrors("email");
        }

        /** @test */
        public function a_user_must_provide_a_password()
        {
          $attributes = factory(User::class)
            ->raw(['password' => '']);

          $this->postJson(route('accounts.register'), $attributes)
            ->assertStatus(422)
            ->assertJsonValidationErrors("password");
        }

        /** @test */
        public function a_users_password_must_be_at_least_strength_level_two()
        {
            $this->assertTrue(true);
        }

        /** @test */
        public function a_user_can_create_their_account()
        {
            $attributes = factory(User::class)
              ->raw();

            $this->postJson(route('accounts.register'), $attributes)
              ->assertStatus(200)
              ->assertJsonStructure(["status","data"]);
        }
    }
