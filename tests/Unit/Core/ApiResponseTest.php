<?php namespace Tests\Unit\Core;
    use Tests\TestCase;
    use Illuminate\Foundation\Testing\WithFaker;
    use Illuminate\Foundation\Testing\RefreshDatabase;
    use App\Traits\RespondsToRequests;
    use App\Models\User;
    use Carbon\Carbon;

    class ApiResponseTest extends TestCase
    {
        use RefreshDatabase;

        private $mock;

        protected function setup(){
            parent::setup();

            $this->mock = $this->getMockForTrait(RespondsToRequests::class);
        }
        /** @test */
        public function it_returns_a_status_message(){
            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $correctResponse = [ 'status' => ['message'   => "Your action was completed successfully."] ];

            $this->assertArraySubset($correctResponse, $responseArray);
        }

        /** @test */
        public function it_returns_a_default_status_code(){
            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $correctResponse = [ 'status' => ['http_code'   => 200] ];

            $this->assertArraySubset($correctResponse, $responseArray);
        }

        /** @test */
        public function it_returns_a_custom_status_code(){
            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->status(403)
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $correctResponse = [ 'status' => ['http_code'   => 403] ];

            $this->assertArraySubset($correctResponse, $responseArray);
        }

        /** @test */
        public function the_successful_attribute_is_false_for_400_and_500_responses(){
            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->status(403)
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $correctResponse = [ 'status' => ['successful' => false] ];

            $this->assertArraySubset($correctResponse, $responseArray);
        }

        /** @test */
        public function the_successful_attribute_is_true_for_200_responses(){
            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $correctResponse = [ 'status' => ['successful' => true] ];

            $this->assertArraySubset($correctResponse, $responseArray);
        }

        /** @test */
        public function it_returns_a_record_when_one_is_provided(){
            $record         = factory(User::class)->create();

            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->record($record)
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $this->assertArrayHasKey("data", $responseArray);
        }

        /** @test */
        public function it_returns_a_collection_when_one_is_provided(){
            $records        = factory(User::class, 2)->create();

            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->collection($records)
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $this->assertArrayHasKey("data", $responseArray);
            $this->assertCount(2, $responseArray['data']);
        }

        /** @test */
        public function if_a_collection_is_paginated_meta_and_links_attributes_are_provided(){
            factory(User::class, 2)->create();

            $records = User::paginate(1);

            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->collection($records)
              ->response();

            $responseArray  = json_decode($response->content(), true);

            $this->assertArrayHasKey("data", $responseArray);
            $this->assertArrayHasKey("meta", $responseArray);
            $this->assertArrayHasKey("links",$responseArray);
            $this->assertCount(1, $responseArray['data']);
        }

        /** @test */
        public function it_returns_errors_if_they_are_provided(){
              $response       = $this->mock
                ->message("Your action was completed successfully.")
                ->errors(['first_name' => "You can't do that"])
                ->response();

              $responseArray  = json_decode($response->content(), true);

              $this->assertArrayHasKey("errors", $responseArray);
        }

        /** @test */
        public function a_valid_bearer_token_response_is_provided(){
            $record         = factory(User::class)->create();
            $token          = $record->createToken('Test Token');

            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->bearerToken($token)
              ->response();

            $responseArray  = json_decode($response->content(), true);
            $correctResponse = [ 'auth' => [
              'type' => 'Bearer',
              'token' => $token->accessToken,
              'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()]
            ];

            $this->assertArrayHasKey("auth", $responseArray);

            $this->assertArraySubset($correctResponse, $responseArray);
        }

        /** @test */
        public function a_valid_two_factor_token_response_is_provided(){
            $record         = factory(User::class)->create();
            $token          = $record->requests()->create([
              'action'      => '2FA',
              'token'       => str_random(25),
              'expires_at'  => Carbon::now()->addWeek()
            ]);

            $response       = $this->mock
              ->message("Your action was completed successfully.")
              ->secondFactorToken($token)
              ->response();

            $responseArray  = json_decode($response->content(), true);
            $correctResponse = [ 'auth' => [
              'type' => '2FA',
              'token' => $token->token,
              'expires_at' => Carbon::parse($token->expires_at)->toDateTimeString()]
            ];

            $this->assertArrayHasKey("auth", $responseArray);

            $this->assertArraySubset($correctResponse, $responseArray);
        }
    }
