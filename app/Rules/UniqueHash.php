<?php namespace App\Rules;
    use Illuminate\Contracts\Validation\Rule;
    use Illuminate\Support\Facades\DB;

    class UniqueHash implements Rule
    {
        private $table;
        private $attribute;
        private $id;

        /**
         * Create a new rule instance.
         *
         * @return void
         */
        public function __construct($table, $id = null)
        {
            $this->table = $table;
            $this->id = $id;
        }

        /**
         * Determine if the validation rule passes.
         *
         * @param  string  $attribute
         * @param  mixed  $value
         * @return bool
         */
        public function passes($attribute, $value)
        {
            $count = DB::table($this->table)
              ->where("_{$attribute}", hash("sha256", $value))
              ->where("id","!=",$this->id)
              ->count();

            $this->attribute = $attribute;

            return $count === 0;
        }

        /**
         * Get the validation error message.
         *
         * @return string
         */
        public function message()
        {
            return "The {$this->attribute} is already taken.";
        }
    }
