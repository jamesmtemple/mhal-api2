<?php namespace App\Traits;
    use Illuminate\Support\Facades\Response;
    use Carbon\Carbon;

    trait RespondsToRequests {
        private $_message         = '';
        private $_code            = 200;
        private $_record          = null;
        private $_collection      = null;
        private $_tokenData       = null;
        private $_meta            = null;
        private $_links           = null;
        private $_errors          = null;
        private $_auth            = null;

        public function message($message){
            $this->_message = $message;

            return $this;
        }

        public function errors($errors){
            $this->_errors = $errors;

            return $this;
        }

        public function status($status){
            $this->_code = $status;

            return $this;
        }

        public function record($record){
            $this->_record = $record;

            return $this;
        }

        public function collection($collection){
            $this->_collection = $collection;

            if($collection instanceof \Illuminate\Pagination\LengthAwarePaginator){
                $this->_collection = $collection->items();

                $this->_meta = [
                    'current_page'        => $collection->currentPage(),
                    'from'                => $collection->firstItem(),
                    'last_page'           => $collection->lastPage(),
                    'path'                => $collection->url($collection->currentPage()),
                    'per_page'            => $collection->perPage(),
                    'to'                  => $collection->lastItem(),
                    'total'               => $collection->total()
                ];

                $this->_links = [
                    'first'               => $collection->url(1),
                    'last'                => $collection->url($collection->lastPage()),
                    'prev'                => $collection->previousPageUrl(),
                    'next'                => $collection->nextPageUrl(),
                ];
            }

            return $this;
        }

        public function bearerToken($token){
            $this->_auth = [
              "type"        => "Bearer",
              "token"       => $token->accessToken,
              'expires_at'  => Carbon::parse($token->token->expires_at)->toDateTimeString()
            ];

            return $this;
        }

        public function secondFactorToken($token){
            $this->_auth = [
              "type"        => "2FA",
              "token"       => $token->token,
              'expires_at'  => Carbon::parse($token->expires_at)->toDateTimeString()
            ];

            return $this;
        }

        public function response(){
            $response = [
                "status"         =>[
                    "successful" => ($this->_code >= 400) ? false : true,
                    "http_code"  => $this->_code,
                    "message"    => $this->_message
                ]
            ];

            if(! is_null($data = $this->getRequestData())){
                $response = array_merge($response, $data);
            }

            if(! is_null($this->_auth)){
                $response = array_merge($response, ['auth' => $this->_auth]);
            }

            if(! is_null($this->_meta)){
                $response = array_merge($response, ['meta' => $this->_meta]);
            }

            if(! is_null($this->_errors)){
                $response = array_merge($response, ['errors' => $this->_errors]);
            }

            if(! is_null($this->_links)){
                $response = array_merge($response, ['links' => $this->_links]);
            }

            return Response::json($response, $this->_code);
        }

        private function getRequestData(){
            if(! is_null($this->_tokenData)){
                return $this->tokenData;
            }

            if(! is_null($this->_record)){
                return ['data' => $this->_record];
            }

            if(! is_null($this->_collection)){
                return ['data' => $this->_collection];
            }
        }
    }
