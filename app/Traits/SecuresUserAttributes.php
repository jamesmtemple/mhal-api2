<?php namespace App\Traits;

    trait SecuresUserAttributes {
        public function setFirstNameAttribute($value){
            $this->attributes['first_name'] = encrypt($value);
        }

        public function setMiddleNameAttribute($value){
            $this->attributes['middle_name'] = (is_null($value)) ? null : encrypt($value);
        }

        public function setLastNameAttribute($value){
            $this->attributes['last_name']  = encrypt($value);
        }

        public function setUsernameAttribute($value){
            $this->attributes['username']  = encrypt($value);
            $this->attributes['_username'] = hash("sha256", $value);
        }

        public function setEmailAttribute($value){
            $this->attributes['email']    = encrypt($value);
            $this->attributes['_email']   = hash("sha256", $value);
        }

        public function setPasswordAttribute($value){
            $this->attributes['password'] = bcrypt($value);
        }

        public function getFirstNameAttribute($value){
            return decrypt($value);
        }

        public function getMiddleNameAttribute($value){
            return (is_null($value)) ? null : decrypt($value);
        }

        public function getLastNameAttribute($value){
            return decrypt($value);
        }

        public function getUsernameAttribute($value){
            return decrypt($value);
        }

        public function getEmailAttribute($value){
            return decrypt($value);
        }
    }
