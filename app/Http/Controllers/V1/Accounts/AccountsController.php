<?php namespace App\Http\Controllers\V1\Accounts;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Rules\UniqueHash;
    use App\Models\User;
    use App\Rules\Password;
    use App\Rules\CurrentPassword;
    use Illuminate\Support\Facades\Auth;

    class AccountsController extends Controller
    {
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $attributes = $request->validate([
                'ein'         => ['required'],
                'email'       => ['required', new UniqueHash('users')],
                'username'    => ['required', new UniqueHash('users')],
                'password'    => ['required', new Password]
            ]);

            $record = User::create($attributes);

            $record->submitRequest("activate");

            return $this
              ->message("Your user account was created successfully. Please check your email for activation instructions!")
              ->record($record)
              ->response();
        }

        /**
         * Display the specified resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function show()
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request)
        {
              $request->validate([
                  'ein'               => ['required'],
                  'email'             => ['required', new UniqueHash('users', Auth::id())],
                  'username'          => ['required', new UniqueHash('users', Auth::id())],
                  'password'          => [new Password],
                  'current_password'  => ['required', new CurrentPassword]
              ]);

              $attributes = $request
                ->only(['ein','email','username']);

              if(is_null($request->password)) {
                $attributes['password'] = $request->password;
              }

              Auth::user()->update($attributes);

              return $this
                ->message("Your account details were updated successfully!")
                ->response();
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function destroy(Request $request)
        {
            //
        }
    }
