<?php namespace App\Http\Controllers\V1\Accounts;
    use Illuminate\Http\Request as HttpRequest;
    use App\Models\Request;
    use App\Http\Controllers\Controller;

    class ActivationsController extends Controller
    {
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, HttpRequest $httpRequest)
        {
            $this->validateUserRequest($request, "activate");

            $request->user
              ->update(['is_verified' => true]);

            return $this
              ->message("Your account was activated successfully!")
              ->response();
        }
    }
