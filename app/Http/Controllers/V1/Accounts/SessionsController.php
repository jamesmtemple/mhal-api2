<?php namespace App\Http\Controllers\V1\Accounts;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Auth;

    class SessionsController extends Controller
    {
        /**
         * Store a newly created resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $request->validate([
                'username'      => ['required'],
                'password'      => ['required']
            ]);

            $credentials = [
              '_username'       => hash("sha256",$request->username),
              'password'        => $request->password,
              'is_verified'     => true
            ];

            if(Auth::attempt($credentials)){
                return $this
                  ->message("You are signed in successfully!")
                  ->bearerToken(Auth::user()->createToken("Personal Access Token"))
                  ->response();
            }

            return $this
              ->status(401)
              ->message("We could not verify the credentials you provided, please try again!")
              ->response();
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
        public function destroy(Request $request)
        {
              $request->user()
                ->token()
                ->revoke();

              return $this
                ->message("You are now signed out!")
                ->response();
        }
    }
