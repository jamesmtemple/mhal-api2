<?php namespace App\Http\Controllers;
    use Illuminate\Foundation\Bus\DispatchesJobs;
    use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use App\Traits\RespondsToRequests;
    use App\Exceptions\InvalidRequestException;
    use Carbon\Carbon;

    class Controller extends BaseController
    {
        use AuthorizesRequests, DispatchesJobs, ValidatesRequests, RespondsToRequests;

        public function validateUserRequest($request, $action){
            if($request->action != $action OR $request->expires_at <= Carbon::now()) {
              throw new InvalidRequestException;
            }
        }
    }
