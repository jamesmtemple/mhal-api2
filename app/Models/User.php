<?php namespace App\Models;
    use Illuminate\Notifications\Notifiable;
    use Illuminate\Contracts\Auth\MustVerifyEmail;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use App\Traits\SecuresUserAttributes;
    use Laravel\Passport\HasApiTokens;
    use Carbon\Carbon;

    class User extends Authenticatable
    {
          use Notifiable, SecuresUserAttributes, HasApiTokens;

          /**
           * The attributes that are mass assignable.
           *
           * @var array
           */
          protected $fillable = [
              'ein',
              'username',
              'email',
              '_username',
              '_email',
              'password',
              'is_verified',
              'backup_code'
          ];

          /**
           * The attributes that should be hidden for arrays.
           *
           * @var array
           */
          protected $hidden = [
              'username',
              '_username',
              '_email',
              'password',
              'remember_token'
          ];

          protected $casts = [
            'is_verified'           => 'boolean',
            'second_factor'         => 'boolean'
          ];

          public function requests(){
              return $this->hasMany(Request::class, "user_id", "id");
          }

          public function submitRequest($action){
              return $this->requests()->create([
                'action'      => $action,
                'expires_at'  => Carbon::now()->addWeek(),
                'token'       => str_random(25)
              ]);
          }
    }
