<?php namespace App\Models;
    use Illuminate\Database\Eloquent\Model;
    use Carbon\Carbon;

    class Request extends Model
    {
        protected $fillable = [
          'action',
          'user_id',
          'token',
          'data',
          'is_completed',
          'expires_at'
        ];

        protected $casts = [
          'is_completed'      => 'boolean'
        ];

        public function setDataAttribute($value){
            $this->attributes['data'] = (is_null($value)) ? null : encrypt(json_encode($value));
        }

        public function getDataAttribute($value){
            return json_decode(decrypt($value));
        }

        public function getRouteKeyName(){
            return 'token';
        }

        public function user(){
            return $this->hasOne(User::class,"id","user_id");
      }
    }
